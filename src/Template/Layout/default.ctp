<?php

$cakeDescription = 'Applications';
?>
<!DOCTYPE html>
<html>
	<head>
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
		</title>
		<?= $this->Html->meta('icon') ?>

		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('script') ?>

		<?= $this->Html->css([
			'sb-admin',
			'sb-admin.min',
			'vendor/bootstrap/css/bootstrap.min',
			'vendor/font-awesome/css/font-awesome.min'
		]);?>

		<?= $this->Html->script([
			'jquery.min',
			'jquery.maskedinput',
			'vendor/popper/popper.min',
			'vendor/bootstrap/js/bootstrap.min',
			'vendor/jquery-easing/jquery.easing.min',
			'vendor/datatables/jquery.dataTables',
			'vendor/datatables/dataTables.bootstrap4',
			'jquery.dataTables.min',
			'dataTables.bootstrap4.min'
		]);?>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

		<!-- Bootstrap Date-Picker Plugin -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

	</head>
	<body>
		<?php echo $this->element('meta/navbar') ?>
		<?= $this->fetch('content') ?>
		<?php echo $this->element('meta/footer') ?>
	</body>
</html>
