<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
	<a class="navbar-brand" href="#">
		<?php echo $this->Html->image('/webroot/img/dar_logo.png',['height' => '30', 'width' => '30']); ?>
		Farmer Beneficiary Tracker
	</a>
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarResponsive">
		<?php		
			$list = [];
			foreach ($menu as $menuItem) {
				$list[] = '
					<a class="nav-link" href="'.$this->Url->build('/'.$menuItem->menu_link, true).'">
						<i class="fa fa-fw '.$menuItem->menu_icon.'"></i>
						<span class="nav-link-text">'.$menuItem->menu_name. '</span>
					</a>
				';
			}
			echo $this->Html->nestedList($list, 
				['class' => 'navbar-nav navbar-sidenav', 'id' => 'exampleAccordion'],
				['class' => 'nav-item','data-toggle' => "tooltip",'data-placement' => 'right']
			);
		?>
		<ul class="navbar-nav sidenav-toggler">
			<li class="nav-item">
				<a class="nav-link text-center" id="sidenavToggler">
					<i class="fa fa-fw fa-angle-left"></i>
				</a>
			</li>
		</ul>
	</div>
</nav>