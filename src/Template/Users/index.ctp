<div class="brclr"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<?= $this->Flash->render() ?>
		<div style="height: 2px"></div>
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<h4 class="text-primary">Farmer Beneficiary</h4>
			</li>
		</ol>
	</div>
	<style type="text/css">
			 #dvMap {
				min-width: 360px;
				width: 100%;
				min-height: 500px;
				height: 80%;
				border: 1px blue;
				z-index: 99999;
			}
	</style>
	<div class="card-body">
		<div class="row">
			<table id="fbtable" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name of FB</th>
						<th>Name of Spouse</th>
						<th>No. of FB</th>
						<th>Postal Address</th>
						<th>CLOA Serial No.</th>
						<th>Lot No.</th>
						<th>Area (Sq. M.)</th>
						<th>Location of Property</th>
						<th>Survey No.</th>
						<th>Former LO</th>
						<th>Program Class</th>
						<th>Date Registered</th>
						<th>OCT/TCT No.</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($users as $user): ?>
					<tr>
						<td><?=h($user->name_of_farmer)?></td>
						<td><?=h($user->name_of_spouse)?></td>
						<td><?=h($user->no_of_fb)?></td>
						<td><?=h($user->postal_address)?></td>
						<td><?=h($user->cloa_serial_no)?></td>
						<td><?=h($user->lot_num)?></td>
						<td><?=h($user->area_sq_mtr)?></td>
						<td><a href="https://www.google.com/maps/search/?api=1&query=<?=h($user->lat_lang)?>" target="_blank"><?=h($user->location_of_property)?></a></td>
						<td><?=h($user->survey_no)?></td>
						<td><?=h($user->former_lo)?></td>
						<td><?=h($user->program_class)?></td>
						<td><?=h($user->date_registered)?></td>
						<td><?=h($user->oct_tct_no)?></td>
						<td>
							<a href="users/edit/<?=h($user->id)?>"><i class="fa fa-eye text-primary"></i></a>
							<?php 
								echo $this->Html->link(
									'<i class="fa fa-trash text-danger" aria-hidden="true"></i>',
									[
										'controller'=>'users',
										'action'=>'delete',
										h($user->id)
									],
									['confirm' => 'Are you sure you want to delete the Farmer Beneficiary?', 'escape' => false]
								);
							?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready( function() {
		$('#fbtable').DataTable();
	});
</script>
