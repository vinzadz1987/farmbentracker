<div class="brclr"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<?= $this->Flash->render() ?>
		<div style="height: 2px"></div>
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<h4 class="text-primary">Update Farmer Beneficiary</h4>
			</li>
			<a href="<?php echo BASE_URL; ?>users"><i class="fa fa-arrow-left pull-right text-primary" style="position: relative;
    left: -7px; top: 9px;"> Back To List</i></a>
		</ol>
	</div>
		<style type="text/css">
			 #dvMap {
				min-width: 360px;
				width: 100%;
				min-height: 500px;
				height: 80%;
				border: 1px blue;
			}
		</style>
		<div class="card-body">
			<?= $this->Form->create($user, [ 'class' => 'form' ]) ?>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->control('name_of_farmer', [ 'placeholder' => 'Enter name of farmer', 'class' => 'form-control' ]); ?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('name_of_spouse', [ 'class' => 'form-control', 'placeholder' => 'Enter name of spouse' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->control('no_of_fb', [ 'placeholder' => 'Enter no. of FB', 'class' => 'form-control', 'type' => 'number' ]); ?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('postal_address', [ 'class' => 'form-control', 'placeholder' => 'Enter Postal address' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->control('cloa_serial_no', [ 'placeholder' => 'Enter CLOA Serial No.', 'class' => 'form-control', 'type' => 'number' ]); ?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('lot_num', [ 'class' => 'form-control', 'placeholder' => 'Enter LOT no.' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->control('area_sq_mtr', [ 'placeholder' => 'Enter Area Sq. meter', 'class' => 'form-control' ]); ?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('location_of_property', [ 'class' => 'form-control', 'placeholder' => 'Enter Location of Property.', 'id' => 'location_of_property' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->control('survey_no', [ 'class' => 'form-control', 'placeholder' => 'Enter survey no' ]) ?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('former_lo', [ 'class' => 'form-control', 'placeholder' => 'Enter Former LO' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-12">
								<?= $this->Form->control('program_class', [ 'class' => 'form-control', 'placeholder' => 'Enter Program class' ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->control('date_registered', [ 'class' => 'form-control', 'id' => 'date_registered', 'type' => 'text', 'placeholder' => 'Enter Date registered' ]) ?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('oct_tct_no', [ 'class' => 'form-control', 'placeholder' => 'OCT/TCT No.' ] ) ?>
							</div>
						</div>
					</div>
			</div>
			<div class="col-md-6"> 
				<div class="form-group">
					<div class="form-row">
						<div class="alert alert-info">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Note!</strong> To mark the actual location, search the actual place and Click the location.
						</div>
						<input type="text" id="query" placeholder="Search LOT Location & Click" style="z-index: 0;position: absolute; bottom: 0px; left: 113px; width: 298px;height: 29px;">
						<div id="dvMap"></div>
					</div>
				</div>
				<?= $this->Form->control('lat_lang', [ 'type' => 'hidden', 'id' => 'latlang']) ?>
				<div style="position: relative;"><?= $this->Form->button(__('Update'),['class'=>'btn btn-primary btn-block col-md-4 pull-right']); ?></div>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCgbUfWIAvt0S4aHjpairELIPGlrZdHKHw"></script>
<script type="text/javascript">
	$(document).ready( function() {
		var date_input=$('#date_registered'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		var options={
			format: 'mm/dd/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		};
		date_input.datepicker(options);
	});

	var marker;

	function initialize() {

		var name_of_farmer = document.getElementById("name-of-farmer").value;
		var cloa_serial_no = document.getElementById("cloa-serial-no").value;
		var location_of_property = document.getElementById("location_of_property").value;
		
		var latlongLocation = document.getElementById("latlang").value;
		var latlongLoc = latlongLocation.slice(1,-1);
		var latLoc = latlongLoc.split(',')[0];
		var langLoc = latlongLoc.split(',')[1];
		var myLatlng = new google.maps.LatLng(latLoc, langLoc);

		var myOptions = {
			zoom: 13,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("dvMap"), myOptions);

		// Create the search box and link it to the UI element.
		var input = document.getElementById('query');
		var searchBox = new google.maps.places.SearchBox(input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		var markers = [];

		marker = new google.maps.Marker({
			draggable: true,
			position: myLatlng,
			map: map,
			title: "Farmer Beneficiary: "+name_of_farmer+ '\r'+'CLOA No : '+cloa_serial_no+'\n Location: '+location_of_property
		});

		google.maps.event.addListener(marker, 'dragend', function(event) {
			document.getElementById("latlang").value = event.latLng.lat() + ',' + event.latLng.lng();
		});

		google.maps.event.addListener(map, 'click', function(event) {
			document.getElementById("latlang").value = event.latLng;
			marker.setPosition(event.latLng);
			// alert(event.latLng);
		});

		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();

			if (places.length == 0) {
				return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];

			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();
			places.forEach(function(place) {
				if (!place.geometry) {
					console.log("Returned place contains no geometry");
					return;
				}
				var icon = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				markers.push(new google.maps.Marker({
					map: map,
					icon: icon,
					title: place.name,
					position: place.geometry.location
				}));

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});
			map.fitBounds(bounds);
		});
	}
	google.maps.event.addDomListener(window, "load", initialize());
</script>
