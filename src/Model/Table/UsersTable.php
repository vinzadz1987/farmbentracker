<?php
	namespace App\Model\Table; 

	use Cake\ORM\Table;
	use Cake\Validation\Validator;
	use Cake\ORM\RulesChecker;
	
	class UsersTable extends Table
	{
		
		public function validationDefault(Validator $validator)
		{
			$validator
				->notEmpty('name_of_farmer', 'A Name of Farmer is required')				
				->notEmpty('name_of_spouse', 'A Name of Spouse is required')
				->notEmpty('no_of_fb', 'A No of FB is required')
				->notEmpty('postal_address', 'A Postal Address is required')
				->notEmpty('cloa_serial_no', 'A Cloa Serial No is required')
				->notEmpty('lot_num', 'A Lot Num is required')
				->notEmpty('area_sq_mtr', 'A area in Sq meter is required')
				->notEmpty('location_of_property', 'A email is required')
				->notEmpty('survey_no', 'A survey no is required')
				->notEmpty('former_lo', 'A former LO is required')
				->notEmpty('program_class', 'A Program Class is required')
				->notEmpty('oct_tct_no', 'OCT/TCT No. is required')
				->notEmpty('date_registered', 'Date registered is required')
				->notEmpty('lat_lang', 'A LatLang no is required');

			return $validator;
		}

	}